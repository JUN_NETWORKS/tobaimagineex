// 画面サイズ
const screenWidth = window.innerWidth
const screenHeight = window.innerHeight

// 読み込み完了時に処理をする
window.onload = () => {

    ymap = new Y.Map("map", {
        configure: {
            doubleClickZoom: true,  // ダブルクリックで地図のズームイン
            scrollWheelZoom: true,  // マウスのスクロールホイールによるズーム処理
            singleClickPan: true,  // シングルクリックで地図を移動
            dragging: true  // ドラッグ操作で地図を移動可能
        }
    });
    ymap.drawMap(new Y.LatLng(34.4868, 136.8431), 17, Y.LayerSetId.NORMAL);
    // ymap.addControl(new Y.LayerSetControl());  // 地図、 航空写真切り替え
    ymap.addControl(new Y.ScaleControl());  // 地図上の距離の目安となるスケールバーを表示
    // ymap.addControl(new Y.SliderZoomControlHorizontal());  // ズーム調整 横
    ymap.addControl(new Y.SliderZoomControlVertical());  // ズーム調整 縦
    // ymap.addControl(new Y.SearchControl());  //地図検索のユーザーインターフェースを表示
    
    // 画面サイズ更新
    $(window).bind("resize", function(e){
        ymap.updateSize();
    });
}
